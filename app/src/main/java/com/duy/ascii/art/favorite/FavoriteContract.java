

package com.duy.ascii.art.favorite;

import com.duy.ascii.art.favorite.localdata.TextItem;

import java.util.ArrayList;

/**
 * Created by Duy on 03-Jul-17.
 */

public class FavoriteContract {
    public interface View {
        void showProgress();

        void hideProgress();

        void display(ArrayList<TextItem> list);

        void setPresenter(Presenter presenter);

        void append(TextItem value);

    }

    public interface Presenter {
        void loadData();

        void stop();
    }

}
