package com.duy.ascii.art.utils

import android.content.Context
import android.content.res.ColorStateList
import android.util.TypedValue
import androidx.annotation.AttrRes
import androidx.annotation.ColorInt
import androidx.core.content.ContextCompat


@ColorInt
fun Context.resolveColorAttr(@AttrRes colorAttr: Int): Int {
    val resolvedAttr = resolveThemeAttr(colorAttr)
    // resourceId is used if it's a ColorStateList, and data if it's a color reference or a hex color
    if (resolvedAttr.resourceId != 0) {
        return ContextCompat.getColor(this, resolvedAttr.resourceId)
    }
    return resolvedAttr.data;
}

fun Context.resolveColorStateListAttr(@AttrRes colorAttr: Int): ColorStateList? {
    val resolvedAttr = resolveThemeAttr(colorAttr)
    // resourceId is used if it's a ColorStateList, and data if it's a color reference or a hex color
    if (resolvedAttr.resourceId != 0) {
        return ContextCompat.getColorStateList(this, resolvedAttr.resourceId)
    }
    return null;
}

fun Context.resolveThemeAttr(@AttrRes attrRes: Int): TypedValue {
    val typedValue = TypedValue()
    theme.resolveAttribute(attrRes, typedValue, true)
    return typedValue
}