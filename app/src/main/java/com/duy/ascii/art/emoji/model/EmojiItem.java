

package com.duy.ascii.art.emoji.model;

import java.io.Serializable;

/**
 * Created by Duy on 1/11/2018.
 */

public class EmojiItem implements Serializable {
    public static final String CHARACTER = "emoji";
    public static final String DESCRIPTION = "desc";
    private String emojiChar;
    private String desc;

    public EmojiItem(String emojiChar, String desc) {

        this.emojiChar = emojiChar;
        this.desc = desc;
    }

    public String getEmojiChar() {
        return emojiChar;
    }

    public void setEmojiChar(String emojiChar) {
        this.emojiChar = emojiChar;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
