

package com.duy.ascii.art.emoticons.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Duy on 1/10/2018.
 */

public class EmoticonCategory implements Serializable {
    public static final String TITLE = "title";
    public static final String DESCRIPTION = "description";
    private String title, description;
    private ArrayList<String> data;

    public EmoticonCategory(String title, String description, ArrayList<String> data) {
        this.title = title;
        this.description = description;
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ArrayList<String> getData() {
        return data;
    }

    public void setData(ArrayList<String> data) {
        this.data = data;
    }
}
