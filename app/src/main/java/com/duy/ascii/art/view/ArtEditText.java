

package com.duy.ascii.art.view;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Duy on 09-Jul-17.
 */

public class ArtEditText extends AppCompatEditText {
    private int row;
    private int col;

    public ArtEditText(Context context) {
        super(context);
    }

    public ArtEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ArtEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void create(int row, int col) {
        this.row = row;
        this.col = col;
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                builder.append(Char.SPACE);
            }
            builder.append("\n");
        }
        setText(builder.toString());
    }

    @Override
    protected void onSelectionChanged(int selStart, int selEnd) {
        super.onSelectionChanged(selStart, selEnd);
        if (selStart == -1 || getText().length() == 0) return;

        if (selStart == getText().length()) selStart--;
        setSelection(selStart, selStart + 1);
    }

}
