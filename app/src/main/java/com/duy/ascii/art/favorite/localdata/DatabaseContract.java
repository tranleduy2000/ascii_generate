

package com.duy.ascii.art.favorite.localdata;

import java.util.ArrayList;

/**
 * Created by Duy on 09-Jul-17.
 */

public class DatabaseContract {
    public interface View {
        void show(ArrayList<TextItem> items);

        void insert(TextItem item, int pos);

        void delete(int pos);
    }

    public interface Presenter {
        void update(TextItem item);

        void delete(TextItem item);

        void insert(TextItem item);

    }
}
