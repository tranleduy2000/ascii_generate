package com.duy.ascii.art.figlet;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.duy.ascii.art.R;
import com.duy.ascii.art.utils.ShareUtil;
import com.flask.colorpicker.builder.ColorPickerClickListener;
import com.flask.colorpicker.builder.ColorPickerDialogBuilder;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by Duy on 15-Jun-17.
 */

public class FigletFragment extends Fragment implements FigletContract.View, FigletAdapter.OnItemClickListener {
    private static final String TAG = "FigletFragment";
    private static final String DEFAULT_TEXT = "ascii";

    private ContentLoadingProgressBar mProgressBar;
    private Dialog dialog;
    private FigletAdapter mAdapter;
    @Nullable
    private FigletContract.Presenter mPresenter;
    private final TextWatcher mInputTextWatcher = new TextWatcher() {

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (mPresenter != null) {
                String text = s.toString();
                if (text.isEmpty()) {
                    text = DEFAULT_TEXT;
                }
                mPresenter.onTextChanged(text);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }
    };
    private EditText mEditIn;

    @NonNull
    public static FigletFragment newInstance() {

        Bundle args = new Bundle();

        FigletFragment fragment = new FigletFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void showResult(@NonNull ArrayList<String> result) {

    }

    @Override
    public void clearResult() {
        mAdapter.clear();
    }

    @Override
    public void addResult(String text) {
        mAdapter.add(text);
    }

    @Override
    public void setPresenter(@Nullable FigletContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void setProgress(int process) {
        mProgressBar.setProgress(process);
    }

    @Override
    public int getMaxProgress() {
        return mProgressBar.getMax();
    }


    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        mProgressBar.hide();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_figlet, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setHasOptionsMenu(true);
        mEditIn = view.findViewById(R.id.edit_in);

        mProgressBar = view.findViewById(R.id.progressBar);
        mProgressBar.setIndeterminate(false);

        RecyclerView mRecyclerView = view.findViewById(R.id.listview);
        mRecyclerView.setHasFixedSize(false);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mAdapter = new FigletAdapter(requireContext(), view.findViewById(R.id.empty_view));
        mAdapter.setOnItemClickListener(this);
        mRecyclerView.setAdapter(mAdapter);
        mEditIn.addTextChangedListener(mInputTextWatcher);

        if (mPresenter == null) {
            mPresenter = new FigletPresenter(requireContext().getAssets(), this);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        mEditIn.setText(sharedPreferences.getString(TAG, ""));
    }


    @Override
    public void onDestroyView() {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        sharedPreferences.edit().putString(TAG, mEditIn.getText().toString()).apply();
        if (mPresenter != null) {
            mPresenter.cancel();
        }
        super.onDestroyView();
    }

    @Override
    public void onShareImage(@NonNull File bitmap) {
        ShareUtil.shareImage(getContext(), bitmap);
    }

    @Override
    public void onDestroy() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
        super.onDestroy();
    }


    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_figlet, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_background_color:
                selectBackgroundColor();
                break;
            case R.id.action_text_color:
                selectTextColor();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectTextColor() {
        if (getContext() == null) {
            return;
        }
        ColorPickerDialogBuilder.with(getContext())
                .setPositiveButton("Select", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int color, Integer[] integers) {
                        mAdapter.setTextColor(color);
                    }
                })
                .initialColor(mAdapter.getTextColor())
                .build()
                .show();
    }

    private void selectBackgroundColor() {
        if (getContext() == null) {
            return;
        }
        ColorPickerDialogBuilder.with(getContext())
                .setPositiveButton("Select", new ColorPickerClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int color, Integer[] integers) {
                        mAdapter.setBackgroundColor(color);
                    }
                })
                .initialColor(mAdapter.getBackgroundColor())
                .build()
                .show();
    }
}
