package com.duy.ascii.art.image.gallery

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.duy.ascii.art.R
import com.duy.ascii.art.image.converter.AsciiFileUtil
import java.io.File

/**
 * Created by Duy on 09-Aug-17.
 */
class GalleryActivity : AppCompatActivity() {

    private var recyclerView: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_gallery)

        setSupportActionBar(findViewById(R.id.toolbar))
        setTitle(R.string.gallery)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        recyclerView = findViewById(R.id.recycle_view)
        recyclerView?.setHasFixedSize(true)
        recyclerView?.layoutManager = LinearLayoutManager(this)
        loadImage()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    private fun loadImage() {
        val folder = AsciiFileUtil.getImageDirectory(this)
        if (folder.exists()) {
            val files = folder.listFiles()
            val paths = ArrayList<File>()
            if (files != null) {
                for (f in files) {
                    if (f.isFile && f.path.endsWith(".png")) {
                        paths.add(f)
                    }
                }
            }
            paths.sortWith { o1, o2 ->
                -java.lang.Long.valueOf(o1.lastModified()).compareTo(o2.lastModified())
            }
            val adapter = GalleryAdapter(this, paths)
            recyclerView!!.adapter = adapter
        } else {
            findViewById<View>(R.id.empty_view).visibility = View.VISIBLE
        }
    }
}