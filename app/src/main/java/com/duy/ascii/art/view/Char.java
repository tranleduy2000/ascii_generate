

package com.duy.ascii.art.view;

public class Char {
    public static final String SPACE = "─";
    public static final String[] TEXTS = {
            "─", "▒", "░", "█", "▄", "▀", "▌", "▐",
            "╔", "╗", "╚", "╝", "╠", "╬", "╣", "╦", "╩", "═", "║"
    };
}