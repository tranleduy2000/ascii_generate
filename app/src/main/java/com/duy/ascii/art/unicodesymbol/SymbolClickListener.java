

package com.duy.ascii.art.unicodesymbol;

 interface SymbolClickListener {
    void onClick(String name);
}