package com.duy.ascii.art.emoticons

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.duy.ascii.art.R
import com.duy.ascii.art.clipboard.ClipboardManagerCompat
import com.duy.ascii.art.clipboard.ClipboardManagerCompatFactory
import com.duy.ascii.art.favorite.localdata.DatabasePresenter
import com.duy.ascii.art.favorite.localdata.TextItem
import com.duy.ascii.art.utils.ShareUtil

/**
 * Created by Duy on 06-May-17.
 */
class EmoticonsAdapter(private val context: Context) :
    RecyclerView.Adapter<EmoticonsAdapter.ViewHolder>() {
    private val items = ArrayList<String>()
    private var inflater: LayoutInflater = LayoutInflater.from(context)
    private val clipboardManagerCompat: ClipboardManagerCompat =
        ClipboardManagerCompatFactory.getManager(context)
    private val databasePresenter: DatabasePresenter = DatabasePresenter(context, null)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.list_item_emoticon, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val text = items[position]
        holder.txtContent.text = text
        holder.txtContent.setOnClickListener {
            clipboardManagerCompat.text = holder.txtContent.text.toString()
            Toast.makeText(context, R.string.copied, Toast.LENGTH_SHORT).show()
        }
        holder.txtContent.setOnLongClickListener {
            ShareUtil.shareText(holder.txtContent.text.toString(), context)
            false
        }
        holder.imgFavorite.setOnClickListener {
            databasePresenter.insert(TextItem(text))
            Toast.makeText(context, R.string.added_to_favorite, Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        items.clear()
        notifyDataSetChanged()
    }

    fun add(value: String) {
        items.add(value)
        notifyItemInserted(items.size - 1)
    }

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(list: ArrayList<String>?) {
        items.clear()
        items.addAll(list!!)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtContent: TextView
        var root: View
        var imgFavorite: View

        init {
            txtContent = itemView.findViewById(R.id.text)
            root = itemView.findViewById(R.id.container)
            imgFavorite = itemView.findViewById(R.id.img_favorite)
        }
    }

    companion object {
        private const val TAG = "ResultAdapter"
    }
}