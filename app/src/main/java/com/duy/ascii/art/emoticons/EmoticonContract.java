

package com.duy.ascii.art.emoticons;

import com.duy.ascii.art.emoticons.model.EmoticonCategory;

import java.util.ArrayList;

/**
 * Created by Duy on 03-Jul-17.
 */

class EmoticonContract {
    public interface View {
        void showProgress();

        void hideProgress();

        void display(ArrayList<EmoticonCategory> list);

        void setPresenter(Presenter presenter);

    }

    public interface Presenter {
        void load(int index);

        void stop();
    }
}
