

package com.duy.ascii.art.view;

import android.content.Context;
import android.util.AttributeSet;

import com.google.android.material.textview.MaterialTextView;

/**
 * Created by Duy on 11-Jul-17.
 */

public class BaseTextView extends MaterialTextView {
    public BaseTextView(Context context) {
        super(context);
        setup(context);
    }

    public BaseTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup(context);
    }

    public BaseTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setup(context);
    }

    private void setup(Context context) {
//        setTypeface(FontManager.getDefault(context.getAssets()));
    }
}
