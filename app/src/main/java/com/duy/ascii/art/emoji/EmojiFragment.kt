package com.duy.ascii.art.emoji

import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.duy.ascii.art.R
import com.duy.ascii.art.SimpleFragment
import com.duy.ascii.art.emoji.model.EmojiCategory

/**
 * Created by Duy on 09-Aug-17.
 */
class EmojiFragment : SimpleFragment() {
    private var mListener: EmojiClickListener? = null

    fun setListener(listener: EmojiClickListener?) {
        mListener = listener
    }

    override fun getRootLayout(): Int {
        return R.layout.fragment_emoji
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val emojis = arguments?.getSerializable(EXTRA_DATA) as EmojiCategory?
        val recyclerView = view.findViewById<RecyclerView>(R.id.recycle_view_emoji)
        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager = GridLayoutManager(activity, 5)
        val emojiAdapter = EmojiAdapter(requireContext(), emojis)
        recyclerView.adapter = emojiAdapter
        emojiAdapter.setListener(mListener)
    }

    companion object {
        const val TAG = "EmojiFragment"
        private const val EXTRA_DATA = "data"
        @JvmStatic
        fun newInstance(emoji: EmojiCategory?): EmojiFragment {
            val args = Bundle()
            args.putSerializable(EXTRA_DATA, emoji)
            val fragment = EmojiFragment()
            fragment.arguments = args
            return fragment
        }
    }
}