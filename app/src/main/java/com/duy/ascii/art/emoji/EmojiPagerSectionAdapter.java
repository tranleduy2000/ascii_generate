

package com.duy.ascii.art.emoji;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.duy.ascii.art.emoji.model.EmojiCategory;

import java.util.ArrayList;

/**
 * Created by Duy on 9/27/2017.
 */
class EmojiPagerSectionAdapter extends FragmentPagerAdapter {
    private ArrayList<EmojiCategory> mEmojis;
    private final EmojiClickListener mListener;

    EmojiPagerSectionAdapter(FragmentManager fm, ArrayList<EmojiCategory> data, EmojiClickListener listener) {
        super(fm);
        this.mEmojis = data;
        this.mListener = listener;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        EmojiFragment emojiFragment = EmojiFragment.newInstance(mEmojis.get(position));
        emojiFragment.setListener(mListener);
        return emojiFragment;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mEmojis.get(position).getName();
    }

    @Override
    public int getCount() {
        return mEmojis.size();
    }
}
