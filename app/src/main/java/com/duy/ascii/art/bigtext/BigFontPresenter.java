

package com.duy.ascii.art.bigtext;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Handler;
import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by Duy on 07-Jul-17.
 */

public class BigFontPresenter implements BigFontContract.Presenter {
    private final BigFontGenerator cache = new BigFontGenerator();
    private final InputStream[] inputStreams;
    private final BigFontContract.View view;
    private final ProcessData process = new ProcessData();
    private final Handler handler = new Handler();

    public BigFontPresenter(InputStream[] inputStream, BigFontContract.View view) {
        this.inputStreams = inputStream;
        this.view = view;
    }

    @Override
    public void convert(String text) {
        handler.removeCallbacks(process);
        process.setInput(text);
        handler.postDelayed(process, 300);
    }

    @Override
    public void cancel() {
        handler.removeCallbacks(process);
        process.cancel();
    }

    @SuppressLint("StaticFieldLeak")
    private class TaskGenerateData extends AsyncTask<String, String, Void> {
        private float maxProgress = 100;
        private final AtomicInteger count = new AtomicInteger(0);
        private final AtomicInteger current = new AtomicInteger(0);
        private final BigFontContract.View view;

        @SuppressWarnings("deprecation")
        public TaskGenerateData(BigFontContract.View view) {
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showProgress();
            maxProgress = view.getMaxProgress();
            view.setProgress(0);
            view.clearResult();
        }

        @Override
        protected Void doInBackground(String... params) {
            if (!cache.isLoaded()) {
                cache.load(inputStreams);
                for (InputStream inputStream : inputStreams) {
                    try {
                        inputStream.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            int size = cache.getSize();

            for (int i = 0; i < size && !isCancelled(); i++) {
                try {
                    String convert = cache.convert(params[0], i);
                    publishProgress(convert);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            if (isCancelled()) return;
            view.addResult(values[0]);
            view.setProgress((int) (maxProgress / count.get() * current.incrementAndGet()));
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            view.hideProgress();
        }
    }

    public class ProcessData implements Runnable {
        @Nullable
        TaskGenerateData taskGenerateData;
        private String input = "";

        public void setInput(String input) {
            this.input = input;
        }

        @Override
        public void run() {
            taskGenerateData = new TaskGenerateData(view);
            taskGenerateData.execute(input);
        }

        public void cancel() {
            if (taskGenerateData != null) {
                taskGenerateData.cancel(true);
            }
        }
    }
}
