

package com.duy.ascii.art.view;

import android.content.Context;
import androidx.appcompat.widget.AppCompatEditText;
import android.util.AttributeSet;

/**
 * Created by Duy on 07-Jul-17.
 */

public class LatinEditText extends AppCompatEditText {
    public LatinEditText(Context context) {
        super(context);
    }

    public LatinEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LatinEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


}
