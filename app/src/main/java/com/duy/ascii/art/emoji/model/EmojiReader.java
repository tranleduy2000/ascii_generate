

package com.duy.ascii.art.emoji.model;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Paint;
import android.os.Build;

import com.duy.ascii.art.database.JsonBridge;
import com.duy.common.utils.DLog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by Duy on 1/11/2018.
 */

public class EmojiReader {
    private static final String TAG = "EmojiReader";

    public static ArrayList<EmojiCategory> readData(Context context) throws IOException, JSONException {
        if (DLog.DEBUG) DLog.d(TAG, "readData() called");
        //the result
        ArrayList<EmojiCategory> emojiList = new ArrayList<>();

        AssetManager assets = context.getAssets();
        String[] fileNames = assets.list("emoji");

        Paint paint = new Paint();

        for (String fileName : fileNames) {
            JSONObject jsonObject = JsonBridge.getJson(assets, "emoji/" + fileName);
            String title = jsonObject.getString(EmojiCategory.TITLE);
            String desc = jsonObject.getString(EmojiCategory.DESCRIPTION);
            EmojiCategory category = new EmojiCategory(title, desc);

            JSONArray jsonArray = jsonObject.getJSONArray(EmojiCategory.DATA);
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject emoji = jsonArray.getJSONObject(i);
                String emojiChar = emoji.getString(EmojiItem.CHARACTER);
                String emojiDesc = emoji.getString(EmojiItem.DESCRIPTION);
                if (paint.hasGlyph(emojiChar)) {
                    category.add(new EmojiItem(emojiChar, emojiDesc));
                }
            }
            emojiList.add(category);
        }
        return emojiList;
    }
}
