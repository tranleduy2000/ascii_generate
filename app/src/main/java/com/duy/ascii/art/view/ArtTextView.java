

package com.duy.ascii.art.view;

import android.content.Context;
import androidx.appcompat.widget.AppCompatTextView;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.View;

/**
 * Created by Duy on 09-Jul-17.
 */

public class ArtTextView extends AppCompatTextView implements View.OnClickListener {
    private int pos = 0;

    public ArtTextView(Context context) {
        super(context);
        init(context);
    }

    public ArtTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ArtTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    private void init(Context context) {
        TypedValue outValue = new TypedValue();
        context.getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        this.setBackgroundResource(outValue.resourceId);
        setOnClickListener(this);
        setText(Char.TEXTS[0]);

        int textSize = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 16f,
                getResources().getDisplayMetrics());
        this.setTextSize(textSize);
    }

    @Override
    public void onClick(View v) {
        pos++;
        if (pos == Char.TEXTS.length) {
            pos = 0;
        }
        setText(Char.TEXTS[pos]);
    }
}
