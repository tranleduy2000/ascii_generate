

package com.duy.ascii.art;

import android.view.MenuItem;

import com.duy.common.ads.StateActivity;

/**
 * Created by Duy on 9/27/2017.
 */

public abstract class BaseActivity extends StateActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
