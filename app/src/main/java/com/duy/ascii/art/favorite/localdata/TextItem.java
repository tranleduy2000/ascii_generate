

package com.duy.ascii.art.favorite.localdata;

/**
 * Created by Duy on 09-Jul-17.
 */

public class TextItem {
    private long time;
    private String text;

    public TextItem(long time, String text) {
        this.time = time;
        this.text = text;
    }

    public TextItem(String text) {
        this.time = System.currentTimeMillis();
        this.text = text;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
