

package com.duy.ascii.art.figlet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by Duy on 15-Jun-17.
 */

public class FigletContract {
    public interface View {
        void showResult(@NonNull ArrayList<String> result);

        void clearResult();

        void addResult(String text);

        void setPresenter(@Nullable Presenter presenter);

        void setProgress(int process);

        int getMaxProgress();


        void showProgress();

        void hideProgress();
    }

    public interface Presenter {
        void onTextChanged(@NonNull String text);

        void cancel();
    }
}
