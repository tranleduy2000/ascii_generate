

package com.duy.ascii.art.emoticons;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.View;

import com.duy.ascii.art.R;
import com.duy.ascii.art.SimpleFragment;
import com.duy.ascii.art.database.JsonBridge;
import com.duy.ascii.art.emoticons.model.EmoticonCategory;
import com.duy.ascii.art.utils.TooltipUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Duy on 9/27/2017.
 */

public class EmoticonsFragment extends SimpleFragment implements EmoticonContract.View, EmoticonCategoriesAdapter.OnCategoryClickListener {
    protected EmoticonContract.Presenter mPresenter;
    protected RecyclerView mCategoriesView, mContentView;
    protected EmoticonCategoriesAdapter mCategoriesAdapter;
    protected EmoticonsAdapter mContentAdapter;
    protected ContentLoadingProgressBar mProgressBar;
    private LoadDataTask mLoadDataTask;
    @Nullable private Toolbar mToolbar;

    @NonNull
    public static EmoticonsFragment newInstance() {

        Bundle args = new Bundle();

        EmoticonsFragment fragment = new EmoticonsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_emoticons;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mToolbar = getActivity().findViewById(R.id.toolbar);
        mCategoriesView = (RecyclerView) findViewById(R.id.recycle_view_header);
        mCategoriesView.setLayoutManager(new LinearLayoutManager(getContext()));
        mCategoriesAdapter = new EmoticonCategoriesAdapter(requireContext());
        mCategoriesView.setAdapter(mCategoriesAdapter);
        mCategoriesView.addItemDecoration(new DividerItemDecoration(requireContext(), DividerItemDecoration.VERTICAL));
        mCategoriesAdapter.setOnCategoryClickListener(this);

        mProgressBar = (ContentLoadingProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.hide();

        mContentView = (RecyclerView) findViewById(R.id.recycle_view_content);
        mContentView.setHasFixedSize(true);
        mContentView.setLayoutManager(new GridLayoutManager(getContext(), 1));
        mContentAdapter = new EmoticonsAdapter(requireContext());
        mContentView.setAdapter(mContentAdapter);

        mLoadDataTask = new LoadDataTask(getContext(), this);
        mLoadDataTask.execute();
    }

    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.hide();
    }

    @Override
    public void display(ArrayList<EmoticonCategory> list) {
        mCategoriesAdapter.setData(list);
        onHeaderClick(list.get(0));
    }

    @Override
    public void setPresenter(EmoticonContract.Presenter presenter) {
        this.mPresenter = presenter;
    }

    @Override
    public void onHeaderClick(EmoticonCategory category) {
        if (mToolbar != null) {
            mToolbar.setSubtitle(category.getTitle());
        }
        mContentAdapter.clear();
        mContentAdapter.addAll(category.getData());
    }

    @Override
    public void onHeaderLongClick(View view, EmoticonCategory category) {
        TooltipUtil.bottomToolTipDialogBox(view, category.getDescription());
    }

    @Override
    public void onDestroyView() {
        if (mLoadDataTask != null) mLoadDataTask.cancel(true);
        super.onDestroyView();
    }

    private static class LoadDataTask extends AsyncTask<Void, Void, ArrayList<EmoticonCategory>> {
        @SuppressLint("StaticFieldLeak")
        private final Context context;
        private final EmoticonContract.View view;

        @SuppressWarnings("deprecation")
        LoadDataTask(Context context, EmoticonContract.View view) {
            this.context = context;
            this.view = view;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            view.showProgress();
        }

        @Override
        protected ArrayList<EmoticonCategory> doInBackground(Void... params) {
            AssetManager assets = context.getAssets();
            ArrayList<EmoticonCategory> categories = new ArrayList<>();
            try {
                String[] files = assets.list("emoticons");
                for (String fileName : files) {
                    if (isCancelled()) break;
                    JSONObject object = JsonBridge.getJson(assets, "emoticons/" + fileName);
                    JSONArray jsonArray = object.getJSONArray("data");
                    ArrayList<String> data = new ArrayList<>();
                    for (int i = 0; i < jsonArray.length(); i++) {
                        data.add(jsonArray.getString(i));
                    }
                    EmoticonCategory item = new EmoticonCategory(
                            object.getString(EmoticonCategory.TITLE),
                            object.getString(EmoticonCategory.DESCRIPTION),
                            data);
                    categories.add(item);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return categories;
        }


        @Override
        protected void onPostExecute(ArrayList<EmoticonCategory> list) {
            super.onPostExecute(list);
            if (isCancelled()) return;
            view.hideProgress();
            view.display(list);
        }
    }
}
