

package com.duy.ascii.art.favorite;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.core.widget.ContentLoadingProgressBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.appcompat.widget.Toolbar;
import android.view.MenuItem;

import com.duy.ascii.art.favorite.localdata.TextItem;
import com.duy.ascii.art.R;

import java.util.ArrayList;

/**
 * Created by Duy on 09-Aug-17.
 */

public class FavoriteActivity extends AppCompatActivity implements FavoriteContract.View {
    public static final int INDEX = 2;
    protected FavoritePresenter mPresenter;
    protected RecyclerView mRecyclerView;
    protected FavoriteAdapter mAdapter;
    protected ContentLoadingProgressBar mProgressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle(R.string.favorite);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mRecyclerView = findViewById(R.id.recycle_view);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(this, DividerItemDecoration.VERTICAL));
        mProgressBar = findViewById(R.id.progress_bar);
        mPresenter = new FavoritePresenter(this, this);
        mAdapter = new FavoriteAdapter(this, mPresenter.getDatabaseHelper());
        mRecyclerView.setAdapter(mAdapter);
        mPresenter.loadData();
    }


    @Override
    public void showProgress() {
        mProgressBar.show();
    }

    @Override
    public void hideProgress() {
        mProgressBar.hide();
    }

    @Override
    public void display(ArrayList<TextItem> list) {
        mAdapter.clear();
        mAdapter.addAll(list);
    }

    @Override
    public void setPresenter(FavoriteContract.Presenter presenter) {
        this.mPresenter = (FavoritePresenter) presenter;
    }

    @Override
    public void append(TextItem value) {
        mAdapter.add(value);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.stop();
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            this.finish();
            return false;
        }
        return super.onOptionsItemSelected(item);
    }
}
