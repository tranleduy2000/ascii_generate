package com.duy.ascii.art.bigtext

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.duy.ascii.art.R
import com.duy.ascii.art.clipboard.ClipboardManagerCompat
import com.duy.ascii.art.clipboard.ClipboardManagerCompatFactory
import com.duy.ascii.art.favorite.localdata.DatabasePresenter
import com.duy.ascii.art.favorite.localdata.TextItem
import com.duy.ascii.art.utils.ShareUtil

/**
 * Created by Duy on 06-May-17.
 */
internal class BigFontAdapter(private val context: Context, emptyView: View?) :
    RecyclerView.Adapter<BigFontAdapter.ViewHolder>() {
    private val objects: MutableList<String> = ArrayList()
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val clipboardManagerCompat: ClipboardManagerCompat = ClipboardManagerCompatFactory.getManager(context)
    private val emptyView: View?
    private val mDatabasePresenter: DatabasePresenter

    init {
        this.emptyView = emptyView
        mDatabasePresenter = DatabasePresenter(context, null)
        invalidateEmptyView()
    }

    private fun invalidateEmptyView() {
        if (objects.size > 0) {
            if (emptyView != null) {
                emptyView.visibility = View.GONE
            }
        } else {
            if (emptyView != null && emptyView.visibility == View.GONE) {
                emptyView.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.list_item_big_font, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val text = objects[position]
        holder.txtContent.text = text
        holder.txtContent.setOnLongClickListener {
            ShareUtil.shareText(text, context)
            false
        }
        holder.txtContent.setOnClickListener {
            clipboardManagerCompat.text = holder.txtContent.text.toString()
            Toast.makeText(context, R.string.copied, Toast.LENGTH_SHORT).show()
        }
        holder.imgCopy.setOnClickListener {
            clipboardManagerCompat.text = holder.txtContent.text.toString()
            Toast.makeText(context, R.string.copied, Toast.LENGTH_SHORT).show()
        }
        holder.imgShare.setOnClickListener {
            ShareUtil.shareText(
                holder.txtContent.text.toString(),
                context
            )
        }
        holder.imgFavorite.setOnClickListener {
            mDatabasePresenter.insert(TextItem(text))
            Toast.makeText(context, R.string.added_to_favorite, Toast.LENGTH_SHORT).show()
        }
    }

    override fun getItemCount(): Int {
        return objects.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        objects.clear()
        notifyDataSetChanged()
        invalidateEmptyView()
    }

    fun add(value: String) {
        objects.add(value)
        notifyItemInserted(objects.size - 1)
        invalidateEmptyView()
    }

    internal class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtContent: TextView
        var imgCopy: View
        var imgShare: View
        var imgFavorite: View

        init {
            txtContent = itemView.findViewById(R.id.text)
            imgCopy = itemView.findViewById(R.id.img_copy)
            imgShare = itemView.findViewById(R.id.img_share)
            imgFavorite = itemView.findViewById(R.id.img_favorite)
        }
    }
}