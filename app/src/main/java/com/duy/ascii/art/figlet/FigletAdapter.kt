package com.duy.ascii.art.figlet

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.Typeface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.duy.ascii.art.ImageFactory
import com.duy.ascii.art.R
import com.duy.ascii.art.clipboard.ClipboardManagerCompat
import com.duy.ascii.art.clipboard.ClipboardManagerCompatFactory
import com.duy.ascii.art.image.converter.AsciiFileUtil
import com.duy.ascii.art.utils.resolveColorAttr
import com.duy.common.media.ImageUtils
import java.io.File
import java.io.IOException

/**
 * Created by Duy on 06-May-17.
 */
internal class FigletAdapter(private val context: Context, emptyView: View?) :
    RecyclerView.Adapter<FigletAdapter.ViewHolder>() {
    private val clipboardManager: ClipboardManagerCompat =
        ClipboardManagerCompatFactory.getManager(context)
    private val items = ArrayList<String>()
    private val inflater = LayoutInflater.from(context)
    private val emptyView: View?
    private var onItemClickListener: OnItemClickListener? = null
    private var backgroundColor: Int
    private var textColor: Int

    init {
        this.emptyView = emptyView
        invalidateEmptyView()
        textColor = context.resolveColorAttr(R.attr.colorOnSurface)
        backgroundColor = context.resolveColorAttr(R.attr.colorSurface)
    }

    private fun invalidateEmptyView() {
        if (items.size > 0) {
            if (emptyView != null) {
                emptyView.visibility = View.GONE
            }
        } else {
            if (emptyView != null && emptyView.visibility == View.GONE) {
                emptyView.visibility = View.VISIBLE
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.list_item_figlet, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txtContent.typeface =
            Typeface.createFromAsset(context.assets, "SpaceMono-Regular.ttf")
        holder.txtContent.text = items[position]
        holder.txtContent.setTextColor(textColor)
        holder.txtContent.setBackgroundColor(backgroundColor)
        holder.imgShare.setOnClickListener {
            if (onItemClickListener != null) {
                try {
                    val file = getImage(holder.txtContent)
                    onItemClickListener!!.onShareImage(file)
                } catch (e: Exception) {
                    e.printStackTrace()
                    //IO exception
                }
            }
        }
        holder.imgCopy.setOnClickListener {
            clipboardManager.text = holder.txtContent.text.toString()
            Toast.makeText(context, R.string.copied, Toast.LENGTH_SHORT).show()
        }
        holder.imgSave.setOnClickListener {
            try {
                val file = getImage(holder.txtContent)
                if (ImageUtils.addImageToGallery(file, context)) {
                    Toast.makeText(context, "Save in " + file.path, Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                e.printStackTrace()
                //IO exception
            }
        }
    }

    @Throws(IOException::class)
    private fun getImage(view: View): File {
        val image = ImageFactory.createImageFromView(view, Color.WHITE)
        val file = File(
            AsciiFileUtil.getImageDirectory(context),
            System.currentTimeMillis().toString() + ".png"
        )
        if (!file.exists()) {
            file.parentFile?.mkdirs()
            file.createNewFile()
        }
        ImageFactory.writeToFile(image, file)
        image.recycle()
        return file
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setOnItemClickListener(onItemClickListener: OnItemClickListener?) {
        this.onItemClickListener = onItemClickListener
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clear() {
        items.clear()
        notifyDataSetChanged()
        invalidateEmptyView()
    }

    fun add(value: String) {
        items.add(value)
        notifyItemInserted(items.size - 1)
        invalidateEmptyView()
    }

    fun getTextColor(): Int {
        return textColor
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setTextColor(textColor: Int) {
        this.textColor = textColor
        notifyDataSetChanged()
    }

    fun getBackgroundColor(): Int {
        return backgroundColor
    }

    @SuppressLint("NotifyDataSetChanged")
    fun setBackgroundColor(backgroundColor: Int) {
        this.backgroundColor = backgroundColor
        notifyDataSetChanged()
    }

    interface OnItemClickListener {
        fun onShareImage(bitmap: File)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtContent: TextView
        var imgShare: View
        var imgCopy: View
        var imgSave: View

        init {
            txtContent = itemView.findViewById(R.id.content)
            imgShare = itemView.findViewById(R.id.img_share)
            imgCopy = itemView.findViewById(R.id.img_copy)
            imgSave = itemView.findViewById(R.id.img_save)
        }
    }
}