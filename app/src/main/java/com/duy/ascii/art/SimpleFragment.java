

package com.duy.ascii.art;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Duy on 9/27/2017.
 */

public abstract class SimpleFragment extends Fragment {
    @Nullable
    private View mRoot;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mRoot = inflater.inflate(getRootLayout(), container, false);
        return mRoot;
    }

    protected View findViewById(int id) {
        if (mRoot != null) {
            return mRoot.findViewById(id);
        }
        return null;
    }

    protected abstract int getRootLayout();
}
