package com.duy.ascii.art.asciiart

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.RecyclerView
import com.duy.ascii.art.BuildConfig
import com.duy.ascii.art.R
import com.duy.ascii.art.asciiart.model.TextArt
import com.duy.ascii.art.clipboard.ClipboardManagerCompat
import com.duy.ascii.art.clipboard.ClipboardManagerCompatFactory
import com.duy.ascii.art.favorite.localdata.DatabasePresenter
import com.duy.ascii.art.favorite.localdata.TextItem
import com.duy.ascii.art.utils.ShareUtil
import java.util.*

/**
 * Created by Duy on 9/27/2017.
 */
class TextArtAdapter(private val mContext: Context) :
    RecyclerView.Adapter<TextArtAdapter.ViewHolder>() {
    private val textArts = ArrayList<TextArt>()
    private val inflater: LayoutInflater = LayoutInflater.from(mContext)
    private val clipboard: ClipboardManagerCompat =
        ClipboardManagerCompatFactory.getManager(mContext)
    private val databasePresenter: DatabasePresenter = DatabasePresenter(mContext, null)
    private var listener: OnItemClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = inflater.inflate(R.layout.list_item_text_art, parent, false)
        return ViewHolder(view)
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = textArts[position]
        val text = item.getContent()
        holder.txtContent.text = text
        holder.imgCopy.setOnClickListener {
            clipboard.text = holder.txtContent.text.toString()
            Toast.makeText(mContext, R.string.copied, Toast.LENGTH_SHORT).show()
        }
        holder.imgShare.setOnClickListener {
            ShareUtil.shareText(
                holder.txtContent.text.toString(),
                mContext
            )
        }
        holder.imgFavorite.setOnClickListener {
            databasePresenter.insert(TextItem(text))
            Toast.makeText(mContext, R.string.added_to_favorite, Toast.LENGTH_SHORT).show()
        }
        if (BuildConfig.DEBUG) {
            holder.btnDelete.visibility = View.VISIBLE
            holder.btnDelete.setOnClickListener { delete(item) }
        } else {
            holder.btnDelete.visibility = View.GONE
            holder.btnDelete.setOnClickListener(null)
        }
    }

    private fun delete(textArt: TextArt) {
        if (listener != null) {
            listener!!.onDelete(textArt)
        }
        val index = textArts.indexOf(textArt)
        if (index >= 0) {
            textArts.removeAt(index)
            notifyItemRemoved(index)
        }
    }

    override fun getItemCount(): Int {
        return textArts.size
    }

    fun add(value: TextArt) {
        textArts.add(value)
        notifyItemInserted(textArts.size - 1)
    }

    val lastItem: TextArt
        get() = textArts[textArts.size - 1]
    val firstItem: TextArt
        get() = textArts[0]

    @SuppressLint("NotifyDataSetChanged")
    fun addAll(textArts: List<TextArt>) {
        //sort decrease
        Collections.sort(textArts) { textArt, t1 -> -textArt.getTime().compareTo(t1.getTime()) }
        this.textArts.addAll(textArts)
        notifyDataSetChanged()
    }

    @SuppressLint("NotifyDataSetChanged")
    fun clearAll() {
        textArts.clear()
        notifyDataSetChanged()
    }

    fun setListener(listener: OnItemClickListener?) {
        this.listener = listener
    }

    interface OnItemClickListener {
        fun onDelete(textArt: TextArt?)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var txtContent: TextView
        var imgCopy: View
        var imgShare: View
        var imgFavorite: View
        var btnDelete: View

        init {
            txtContent = itemView.findViewById(R.id.txt_content)
            imgCopy = itemView.findViewById(R.id.img_copy)
            imgShare = itemView.findViewById(R.id.img_share)
            imgFavorite = itemView.findViewById(R.id.img_favorite)
            btnDelete = itemView.findViewById(R.id.btn_delete)
        }
    }
}