

package com.duy.ascii.art.database;

import androidx.annotation.Nullable;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

/**
 * Created by Duy on 1/11/2018.
 */

public class JavaSerialObject {
    public static boolean writeObject(Object o, OutputStream outputStream) {
        ObjectOutputStream objectOutputStream = null;
        try {
            objectOutputStream = new ObjectOutputStream(outputStream);
            objectOutputStream.writeObject(o);
            objectOutputStream.flush();
            objectOutputStream.close();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Nullable
    public static Object readObject(InputStream inputStream) {
        try {
            ObjectInputStream in = null;
            in = new ObjectInputStream(inputStream);
            Object o = in.readObject();
            in.close();
            return o;
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}
