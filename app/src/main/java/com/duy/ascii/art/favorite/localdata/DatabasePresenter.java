

package com.duy.ascii.art.favorite.localdata;

import android.content.Context;
import androidx.annotation.Nullable;

/**
 * Created by Duy on 09-Jul-17.
 */

public class DatabasePresenter implements DatabaseContract.Presenter {
    private final Context context;
    @Nullable
    private final DatabaseContract.View view;
    private final DatabaseHelper mDatabaseHelper;

    public DatabasePresenter(Context context, @Nullable DatabaseContract.View view) {
        this.context = context;
        this.view = view;
        mDatabaseHelper = new DatabaseHelper(context);
    }

    @Override
    public void update(TextItem item) {
    }

    @Override
    public void delete(TextItem item) {
        mDatabaseHelper.delete(item);
    }

    @Override
    public void insert(TextItem item) {
        mDatabaseHelper.insert(item);
    }

}
