

package com.duy.ascii.art.purchase;

import android.content.Context;

import com.duy.common.purchase.Premium;

/**
 * Created by Duy on 28-Dec-17.
 */

public class AsciiPremium {
    public static boolean isPremiumUser(Context context) {
        return isProPackage(context) || isPurchase(context);
    }

    private static boolean isProPackage(Context context) {
        return context.getPackageName().equals("com.duy.asciigenerator.pro");
    }

    private static boolean isPurchase(Context context) {
        return Premium.isPremiumUser(context);
    }

    public static boolean isFreeUser(Context context) {
        return !isPremiumUser(context);
    }
}
