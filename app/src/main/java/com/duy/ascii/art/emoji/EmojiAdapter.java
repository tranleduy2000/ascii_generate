

package com.duy.ascii.art.emoji;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.duy.ascii.art.R;
import com.duy.ascii.art.clipboard.ClipboardManagerCompat;
import com.duy.ascii.art.clipboard.ClipboardManagerCompatFactory;
import com.duy.ascii.art.emoji.model.EmojiCategory;
import com.duy.ascii.art.emoji.model.EmojiItem;


/**
 * Created by Duy on 06-May-17.
 */

class EmojiAdapter extends RecyclerView.Adapter<EmojiAdapter.ViewHolder> {
    protected LayoutInflater mInflater;
    private final Context mContext;
    private final ClipboardManagerCompat mClipboardManagerCompat;
    private final EmojiCategory mCategory;
    @Nullable
    private EmojiClickListener mListener;

    EmojiAdapter(@NonNull Context context, EmojiCategory category) {
        this.mContext = context;
        this.mInflater = LayoutInflater.from(context);
        this.mClipboardManagerCompat = ClipboardManagerCompatFactory.getManager(context);
        this.mCategory = category;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.list_item_emoji, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final EmojiItem emojiItem = mCategory.get(position);
        holder.txtContent.setText(mCategory.get(position).getEmojiChar());
        holder.txtContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                if (mListener != null) {
                    mListener.onLongClick(holder.txtContent, emojiItem);
                }
                return true;
            }
        });
        holder.txtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onClick(emojiItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mCategory.size();
    }

    public void setListener(@Nullable EmojiClickListener listener) {
        this.mListener = listener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtContent;

        public ViewHolder(View itemView) {
            super(itemView);
            txtContent = itemView.findViewById(R.id.text);
        }

    }
}

