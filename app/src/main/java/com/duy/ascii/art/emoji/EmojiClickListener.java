

package com.duy.ascii.art.emoji;

import android.view.View;

import com.duy.ascii.art.emoji.model.EmojiItem;

public interface EmojiClickListener {
    void onClick(EmojiItem emojiItem);

    void onLongClick(View view, EmojiItem emojiItem);
}