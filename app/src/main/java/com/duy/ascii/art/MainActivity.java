package com.duy.ascii.art;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentTransaction;

import com.duy.ascii.art.asciiart.TextArtFragment;
import com.duy.ascii.art.bigtext.BigFontFragment;
import com.duy.ascii.art.emoji.EmojiCategoriesFragment;
import com.duy.ascii.art.emoticons.EmoticonsFragment;
import com.duy.ascii.art.favorite.FavoriteActivity;
import com.duy.ascii.art.figlet.FigletFragment;
import com.duy.ascii.art.image.ImageToAsciiFragment;
import com.duy.ascii.art.unicodesymbol.SymbolFragment;
import com.duy.common.utils.ShareUtil;
import com.duy.common.utils.StoreUtil;
import com.google.android.material.navigation.NavigationView;
import com.kobakei.ratethisapp.RateThisApp;


/**
 * Created by Duy on 09-Aug-17.
 */

public class MainActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {
    NavigationView mNavigationView;
    private DrawerLayout mDrawerLayout;
    private Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(findViewById(R.id.toolbar));
        setTitle(R.string.app_name);

        bindView();

        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction
                .replace(R.id.content, TextArtFragment.newInstance())
                .commit();
        toolbar.setSubtitle(R.string.text_art);

        showDialogRate();
    }

    private void showDialogRate() {
        // Monitor launch times and interval from installation
        RateThisApp.onCreate(this);
        // If the condition is satisfied, "Rate this app" dialog will be shown
        RateThisApp.showRateDialogIfNeeded(this);
    }

    private void bindView() {
        toolbar = findViewById(R.id.toolbar);
        mNavigationView = findViewById(R.id.navigation_view);
        mNavigationView.setNavigationItemSelectedListener(this);

        mNavigationView.getMenu().findItem(R.id.action_text_art).setChecked(true);


        mDrawerLayout = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar,
                R.string.open_drawer, R.string.close_drawer);
        mDrawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_favorite) {
            startActivity(new Intent(this, FavoriteActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            super.onBackPressed();
        } else {
            mDrawerLayout.openDrawer(GravityCompat.START);
        }
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        mDrawerLayout.closeDrawers();
        if (mNavigationView != null) {
            Menu menu = mNavigationView.getMenu();
            for (int i = 0; i < menu.size(); i++) {
                menu.getItem(i).setChecked(false);
            }
        }
        switch (item.getItemId()) {
            case R.id.action_text_art:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, TextArtFragment.newInstance()).commit();
                toolbar.setSubtitle(R.string.text_art);
                item.setChecked(true);
                break;
            case R.id.action_big_text:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, BigFontFragment.newInstance()).commit();
                toolbar.setSubtitle(R.string.big_text);
                item.setChecked(true);
                break;
            case R.id.action_image_to_ascii:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, ImageToAsciiFragment.newInstance()).commit();
                toolbar.setSubtitle(R.string.image_to_ascii);
                item.setChecked(true);
                break;
            case R.id.action_emoji:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, EmojiCategoriesFragment.newInstance()).commit();
                toolbar.setSubtitle(R.string.emoji);
                item.setChecked(true);
                break;
            case R.id.action_emoticon:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, EmoticonsFragment.newInstance()).commit();
                toolbar.setSubtitle(R.string.emoticons);
                item.setChecked(true);
                break;
            case R.id.action_symbol:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, SymbolFragment.newInstance()).commit();
                toolbar.setSubtitle(R.string.cool_symbol);
                item.setChecked(true);
                break;
            case R.id.action_figlet:
                toolbar.setSubtitle(R.string.cool_symbol);
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.content, FigletFragment.newInstance()).commit();
                item.setChecked(true);
                break;
            case R.id.action_rate:
                StoreUtil.gotoPlayStore(MainActivity.this, BuildConfig.APPLICATION_ID);
                return true;
            case R.id.action_share:
                ShareUtil.shareThisApp(MainActivity.this);
                return true;
        }
        return false;
    }
}
