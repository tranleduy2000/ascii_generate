

package com.duy.ascii.art;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.view.View;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Duy on 02-Jun-17.
 */

public class ImageFactory {

    public static Bitmap createImageFromView(View view, int background) {
        Bitmap b = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(b);
        Drawable bg = view.getBackground();
        if (bg == null) {
            canvas.drawColor(background);
        }
        view.draw(canvas);
        return b;
    }

    public static Uri writeToFile(Bitmap bitmap, File out) throws IOException {
        FileOutputStream fileOutputStream = new FileOutputStream(out);
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, fileOutputStream);
        fileOutputStream.close();
        return Uri.fromFile(out);
    }
}
