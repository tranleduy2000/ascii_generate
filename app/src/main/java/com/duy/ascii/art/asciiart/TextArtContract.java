

package com.duy.ascii.art.asciiart;

import com.duy.ascii.art.asciiart.model.TextArt;

import java.util.ArrayList;

/**
 * Created by Duy on 03-Jul-17.
 */
class TextArtContract {
    public interface View {
        void showProgress();

        void hideProgress();

        void display(ArrayList<TextArt> list);

        void setPresenter(Presenter presenter);

        void append(TextArt value);
    }

    public interface Presenter {
        void onStart();

        void onStop();
    }
}
