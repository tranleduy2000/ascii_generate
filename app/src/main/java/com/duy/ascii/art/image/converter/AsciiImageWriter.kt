package com.duy.ascii.art.image.converter

import android.content.Context
import android.graphics.Bitmap
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Writes bitmaps and HTML to directories on the external storage directory.
 */
object AsciiImageWriter {

    private val filenameDateFormat: DateFormat = SimpleDateFormat("yyyy_MM_dd_HH_mm_ss", Locale.US)


    @JvmStatic
    @Throws(IOException::class)
    fun saveImage(context: Context?, image: Bitmap): String {
        val dateStr = filenameDateFormat.format(Date())
        val imageFile = File(AsciiFileUtil.getImageDirectory(context!!), "$dateStr.png")
        if (!imageFile.exists()) {
            imageFile.parentFile?.mkdirs()
        }
        saveBitmap(image, imageFile)
        return imageFile.path
    }

    @Throws(IOException::class)
    fun saveBitmap(bitmap: Bitmap, fileToWrite: File): Boolean {
        var output: FileOutputStream? = null
        try {
            output = FileOutputStream(fileToWrite)
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, output)
            output.close()
        } catch (e: Exception) {
            return false
        } finally {
            output?.close()
        }
        return true
    }
}