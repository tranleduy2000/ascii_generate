

package com.duy.ascii.art.unicodesymbol;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.duy.ascii.art.R;
import com.duy.ascii.art.clipboard.ClipboardManagerCompat;
import com.duy.ascii.art.clipboard.ClipboardManagerCompatFactory;

import java.util.ArrayList;


/**
 * Created by Duy on 06-May-17.
 */

class SymbolAdapter extends RecyclerView.Adapter<SymbolAdapter.ViewHolder> {
    protected LayoutInflater inflater;
    private final Context context;
    private final ClipboardManagerCompat clipboardManagerCompat;
    private final ArrayList<String> symbols;
    @Nullable
    private SymbolClickListener mListener;

    SymbolAdapter(@NonNull Context context, ArrayList<String> symbols) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.clipboardManagerCompat = ClipboardManagerCompatFactory.getManager(context);
        this.symbols = symbols;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.list_item_emoji, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.txtContent.setText(symbols.get(position));
        holder.txtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                clipboardManagerCompat.setText(holder.txtContent.getText().toString());
                Toast.makeText(context, R.string.copied, Toast.LENGTH_SHORT).show();
            }
        });
        holder.txtContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) mListener.onClick(symbols.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return symbols.size();
    }

    public void setListener(@Nullable SymbolClickListener listener) {
        this.mListener = listener;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtContent;

        public ViewHolder(View itemView) {
            super(itemView);
            txtContent = itemView.findViewById(R.id.text);
        }

    }
}

