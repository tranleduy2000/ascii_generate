

package com.duy.ascii.art.favorite;

import android.content.Context;
import android.os.AsyncTask;

import com.duy.ascii.art.favorite.localdata.DatabaseHelper;
import com.duy.ascii.art.favorite.localdata.TextItem;

import java.util.ArrayList;

/**
 * Created by Duy on 03-Jul-17.
 */

public class FavoritePresenter implements FavoriteContract.Presenter {
    private final Context context;
    private final FavoriteContract.View view;
    private AsyncTask<String, String, ArrayList<TextItem>> mLoadData;
    private final DatabaseHelper mDatabaseHelper;

    public FavoritePresenter(Context context, FavoriteContract.View view) {
        this.context = context;
        this.view = view;
        this.mDatabaseHelper = new DatabaseHelper(context);
    }

    public DatabaseHelper getDatabaseHelper() {
        return mDatabaseHelper;
    }

    @Override
    public void loadData() {
        view.showProgress();
        if (mLoadData != null && !mLoadData.isCancelled()) {
            mLoadData.cancel(true);
        }
        LoadDataTask.Callback finish = new LoadDataTask.Callback() {
            @Override
            public void onResult(ArrayList<TextItem> list) {
                view.hideProgress();
                view.display(list);
            }
        };
        mLoadData = new LoadDataTask(finish, view, mDatabaseHelper);
        mLoadData.execute();
    }

    @Override
    public void stop() {
        if (mLoadData != null) {
            mLoadData.cancel(true);
        }
    }

    private static class LoadDataTask extends AsyncTask<String, String, ArrayList<TextItem>> {
        private final Callback callback;
        private final FavoriteContract.View view;
        private final DatabaseHelper mDatabaseHelper;

        LoadDataTask(Callback callback, FavoriteContract.View view, DatabaseHelper mDatabaseHelper) {
            this.callback = callback;
            this.view = view;
            this.mDatabaseHelper = mDatabaseHelper;
        }

        @Override
        protected ArrayList<TextItem> doInBackground(String... params) {
            return mDatabaseHelper.getAll();
        }


        @Override
        protected void onPostExecute(ArrayList<TextItem> list) {
            super.onPostExecute(list);
            if (!isCancelled()) {
                callback.onResult(list);
            }
        }

        interface Callback {
            void onResult(ArrayList<TextItem> list);
        }
    }
}
