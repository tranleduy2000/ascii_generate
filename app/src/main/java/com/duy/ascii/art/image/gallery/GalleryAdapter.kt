package com.duy.ascii.art.image.gallery

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.duy.ascii.art.R
import com.duy.ascii.art.utils.ShareUtil
import java.io.File

/**
 * Created by Duy on 09-Aug-17.
 */
class GalleryAdapter(private val context: Context, paths: ArrayList<File>) :
    RecyclerView.Adapter<GalleryAdapter.ViewHolder>() {

    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private var paths = ArrayList<File>()

    init {
        this.paths = paths
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(inflater.inflate(R.layout.list_item_gallery, parent, false))
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        Glide.with(context).load(paths[position])
            .apply(RequestOptions().centerCrop())
            .into(holder.imageView)
        holder.btnDelete.setOnClickListener { remove(holder.adapterPosition) }
        holder.btnShare.setOnClickListener {
            ShareUtil.shareImage(
                context,
                paths[holder.adapterPosition]
            )
        }
    }

    private fun remove(adapterPosition: Int) {
        try {
            val remove = paths.removeAt(adapterPosition)
            if (remove.delete()) {
                notifyItemRemoved(adapterPosition)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getItemCount(): Int {
        return paths.size
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imageView: ImageView
        var btnDelete: Button
        var btnShare: Button

        init {
            imageView = itemView.findViewById(R.id.image_view)
            btnDelete = itemView.findViewById(R.id.btn_delete)
            btnShare = itemView.findViewById(R.id.btn_share)
        }
    }
}