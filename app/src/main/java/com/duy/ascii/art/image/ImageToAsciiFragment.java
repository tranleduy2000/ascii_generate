package com.duy.ascii.art.image;

import static android.app.Activity.RESULT_OK;
import static com.duy.ascii.art.image.converter.ProcessImageOperation.processImage;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.activity.result.ActivityResult;
import androidx.activity.result.ActivityResultCallback;
import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;

import com.duy.ascii.art.R;
import com.duy.ascii.art.SimpleFragment;
import com.duy.ascii.art.image.converter.AsciiConverter;
import com.duy.ascii.art.image.converter.MediaUtils;
import com.duy.ascii.art.image.gallery.GalleryActivity;
import com.duy.ascii.art.utils.ShareUtil;

import java.io.File;
import java.io.IOException;

/**
 * Created by Duy on 9/27/2017.
 */

public class ImageToAsciiFragment extends SimpleFragment {
    private static final int TAKE_PICTURE = 1;
    private static final int REQUEST_READ_IMAGE = 1002;
    private final MutableLiveData<File> resultFileLiveData = new MutableLiveData<>(null);
    private ImageView mPreview;
    private ProgressBar mProgressBar;
    private Spinner mSpinnerType;
    private Uri mOriginalUri = null;

    private final ActivityResultLauncher<Intent> selectImageLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            new ActivityResultCallback<ActivityResult>() {
                @Override
                public void onActivityResult(ActivityResult result) {
                    int resultCode = result.getResultCode();
                    Intent intent = result.getData();
                    if (resultCode == Activity.RESULT_OK) {
                        // There are no request codes
                        if (intent != null && intent.getData() != null) {
                            mOriginalUri = intent.getData();
                            mPreview.setImageBitmap(null);
                            convertImageToAsciiFromIntent(intent.getData());
                        } else {
                            mOriginalUri = null;
                            resultFileLiveData.postValue(null);
                            mPreview.setImageBitmap(null);
                        }
                    }
                }
            });

    @NonNull
    public static ImageToAsciiFragment newInstance() {

        Bundle args = new Bundle();

        ImageToAsciiFragment fragment = new ImageToAsciiFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    protected int getRootLayout() {
        return R.layout.fragment_image_to_ascii;
    }

    private void selectImage() {

        try {
            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            selectImageLauncher.launch(Intent.createChooser(intent, "Select Picture"));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private boolean isReadImagePermissionGranted() {
        Context context = requireContext();
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU) {
            String permission = Manifest.permission.READ_MEDIA_IMAGES;
            int result = ActivityCompat.checkSelfPermission(context, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        } else {
            String permission = Manifest.permission.READ_EXTERNAL_STORAGE;
            int result = ActivityCompat.checkSelfPermission(context, permission);
            return result == PackageManager.PERMISSION_GRANTED;
        }

    }

    private void requestReadImagePermission() {
        String[] permissions;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.TIRAMISU) {
            permissions = new String[]{Manifest.permission.READ_MEDIA_IMAGES};
        } else {
            permissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        }
        requestPermissions(permissions, REQUEST_READ_IMAGE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        if (requestCode == TAKE_PICTURE) {
            this.mOriginalUri = intent.getData();
            if (resultCode == RESULT_OK) {
                if (intent.getData() != null) {
                    convertImageToAsciiFromIntent(intent.getData());
                } else {
                    Toast.makeText(getContext(), "Capture failed", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (isReadImagePermissionGranted()) {
            selectImage();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("result_file")) {
                resultFileLiveData.postValue((File) savedInstanceState.getSerializable("result_file"));
            }
            mOriginalUri = savedInstanceState.getParcelable("origin_uri");
        }

        mPreview = (ImageView) findViewById(R.id.image_preview);
        findViewById(R.id.btn_select).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectButtonClicked();
            }
        });
        mProgressBar = (ProgressBar) findViewById(R.id.progress_bar);
        mProgressBar.setVisibility(View.GONE);
        mSpinnerType = (Spinner) findViewById(R.id.spinner_type);
        mSpinnerType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (mOriginalUri != null) {
                    convertImageToAsciiFromIntent(mOriginalUri);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        View containerAction = view.findViewById(R.id.image_action_container);
        resultFileLiveData.observe(getViewLifecycleOwner(), new Observer<File>() {
            @Override
            public void onChanged(File file) {
                containerAction.setVisibility(file == null ? View.GONE : View.VISIBLE);
            }
        });
        containerAction.findViewById(R.id.btn_share).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                shareCurrentImage();
            }
        });
        containerAction.findViewById(R.id.btn_save).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveImage();
            }
        });
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mOriginalUri != null) outState.putParcelable("origin_uri", mOriginalUri);
        if (resultFileLiveData.getValue() != null)
            outState.putSerializable("result_file", resultFileLiveData.getValue());
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_image_to_ascii, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @SuppressLint("NonConstantResourceId")
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int i = item.getItemId();
        switch (i) {
            case R.id.action_share:
                shareCurrentImage();
                return true;
            case R.id.action_gallery:
                startActivity(new Intent(getContext(), GalleryActivity.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void convertImageToAsciiFromIntent(Uri uri) {
        this.resultFileLiveData.postValue(null);
        new TaskConvertImageToAscii(getContext(), getCurrentType()).execute(uri);
    }

    private AsciiConverter.ColorType getCurrentType() {
        switch (mSpinnerType.getSelectedItemPosition()) {
            case 1:
                return AsciiConverter.ColorType.FULL_COLOR;
            case 2:
                return AsciiConverter.ColorType.NONE;
            default:
                return AsciiConverter.ColorType.ANSI_COLOR;
        }
    }


    private void selectButtonClicked() {
        if (!isReadImagePermissionGranted()) {
            requestReadImagePermission();
        } else {
            selectImage();
        }
    }

    private void saveImage() {
        if (resultFileLiveData.getValue() != null) {
            addImageToGallery(resultFileLiveData.getValue());
        } else {
            Toast.makeText(getContext(), R.string.null_uri, Toast.LENGTH_SHORT).show();
        }
    }

    private void shareCurrentImage() {
        if (resultFileLiveData.getValue() == null) {
            Toast.makeText(getContext(), R.string.null_uri, Toast.LENGTH_SHORT).show();
        } else {
            ShareUtil.shareImage(getContext(), resultFileLiveData.getValue());
        }
    }

    public void addImageToGallery(final File file) {
        if (getContext() == null) {
            return;
        }
        if (!isWritePermissionGranted()) {
            //noinspection deprecation
            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 23);
            return;
        }
        try {
            String folderName = "AsciiArt";
            MediaUtils.saveImage(BitmapFactory.decodeFile(file.getPath()),
                    getContext(), folderName, file.getName(), Bitmap.CompressFormat.PNG);
            Toast.makeText(getContext(), R.string.saved_in_gallery, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private boolean isWritePermissionGranted() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            return true;
        }
        int result = ActivityCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    @SuppressWarnings("deprecation")
    @SuppressLint("StaticFieldLeak")
    private class TaskConvertImageToAscii extends AsyncTask<Uri, Void, File> {
        private final Context context;
        private final AsciiConverter.ColorType type;

        TaskConvertImageToAscii(Context context, AsciiConverter.ColorType type) {
            this.context = context;
            this.type = type;
        }

        @Override
        protected File doInBackground(Uri... params) {
            try {
                String output = processImage(context, params[0], type);
                if (output != null) {
                    return new File(output);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onPostExecute(final File file) {
            super.onPostExecute(file);
            if (file == null) {
                Toast.makeText(context, "IO Exception", Toast.LENGTH_SHORT).show();
            } else {
                mPreview.setImageURI(Uri.fromFile(file));
                resultFileLiveData.postValue(file);
            }
            mProgressBar.setVisibility(View.GONE);
        }

    }
}
