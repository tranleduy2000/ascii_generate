package com.duy.ascii.art.image.converter

import android.content.ContentValues
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import java.io.File
import java.io.FileNotFoundException
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*

object MediaUtils {
    @JvmStatic
    @Throws(FileNotFoundException::class)
    fun saveImage(
        bitmap: Bitmap,
        context: Context,
        folderName: String,
        fileName: String,
        compressionFormat: CompressFormat
    ) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {

            val values = ContentValues()
            values.put(MediaStore.Images.Media.RELATIVE_PATH, "Pictures/$folderName/${fileName}")
            values.put(MediaStore.Images.Media.IS_PENDING, true)

            // RELATIVE_PATH and IS_PENDING are introduced in API 29.

            val uri: Uri? = context.contentResolver
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

            if (uri != null) {
                saveImageToStream(bitmap, context.contentResolver.openOutputStream(uri), compressionFormat)
                values.put(MediaStore.Images.Media.IS_PENDING, false)
                context.contentResolver.update(uri, values, null, null)
            }

        } else {

            val picturesDir = File(
                context.getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!,
                folderName
            );

            // getExternalStorageDirectory is deprecated in API 29

            if (!picturesDir.exists()) {
                picturesDir.mkdirs()
            }


            val imageFile = File(picturesDir.absolutePath, fileName)
            saveImageToStream(bitmap, FileOutputStream(imageFile), compressionFormat)

            val values = ContentValues()
            values.put(MediaStore.Images.Media.DATA, imageFile.absolutePath)
            // .DATA is deprecated in API 29
            context.contentResolver
                .insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)

        }
    }

    private fun contentValues(): ContentValues {

        val values = ContentValues()

        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg")
        values.put(MediaStore.Images.Media.DATE_ADDED, System.currentTimeMillis() / 1000)
        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis())

        return values

    }

    private fun saveImageToStream(
        bitmap: Bitmap, outputStream: OutputStream?,
        compressionFormat: Bitmap.CompressFormat = Bitmap.CompressFormat.JPEG
    ) {
        if (outputStream == null) {
            return;
        }
        bitmap.compress(compressionFormat, 100, outputStream)
        outputStream.close()
    }
}