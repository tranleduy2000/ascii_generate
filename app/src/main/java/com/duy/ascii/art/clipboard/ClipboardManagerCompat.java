

package com.duy.ascii.art.clipboard;

public interface ClipboardManagerCompat {
    CharSequence getText();

    void setText(CharSequence text);

    boolean hasText();
}
