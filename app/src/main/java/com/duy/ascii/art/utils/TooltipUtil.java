

package com.duy.ascii.art.utils;

import android.view.View;

import it.sephiroth.android.library.tooltip.Tooltip;

/**
 * Created by Duy on 1/11/2018.
 */

public class TooltipUtil {

    public static void bottomToolTipDialogBox(View view, String description) {
        Tooltip.Builder builder = new Tooltip.Builder(101)
                .anchor(view, Tooltip.Gravity.BOTTOM)
                .closePolicy(new Tooltip.ClosePolicy()
                        .insidePolicy(true, false)
                        .outsidePolicy(true, false), 4000)
                .activateDelay(900)
                .showDelay(400)
                .text(description)
                .maxWidth(600)
                .withArrow(true)
                .withOverlay(true);
        Tooltip.make(view.getContext(), builder.build()).show();
    }

}
