

package com.duy.ascii.art.bigtext;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

/**
 * Created by Duy on 07-Jul-17.
 */

public class BigFontContract {

    public interface View {
        void showResult(@NonNull ArrayList<String> result);

        void clearResult();

        void addResult(String text);

        void setPresenter(@Nullable Presenter presenter);

        void setProgress(int process);

        int getMaxProgress();

        void setColor(int color);

        void showProgress();

        void hideProgress();
    }

    public interface Presenter {
        void convert(String text);

        void cancel();
    }
}
