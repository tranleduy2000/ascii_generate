package com.duy.common.ads;

import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by Duy on 25-Dec-17.
 */

public class StateActivity extends AppCompatActivity {
    /**
     * Activity on top of screen
     */
    private boolean isVisible = true;

    @Override
    protected void onPause() {
        super.onPause();
        isVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    public boolean isActivityVisible() {
        return isVisible;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
