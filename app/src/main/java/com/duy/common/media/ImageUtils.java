

package com.duy.common.media;

import android.content.ContentValues;
import android.content.Context;

import java.io.File;

import static android.provider.MediaStore.Images;
import static android.provider.MediaStore.MediaColumns;

/**
 * Created by Duy on 28-Dec-17.
 */

public class ImageUtils {
    /**
     * store image file on media store provider
     */
    public static boolean addImageToGallery(final String filePath, final Context context) {
        try {
            ContentValues values = new ContentValues();

            values.put(Images.Media.DATE_TAKEN, System.currentTimeMillis());
            values.put(Images.Media.MIME_TYPE, "image/jpeg");
            values.put(MediaColumns.DATA, filePath);

            context.getContentResolver().insert(Images.Media.EXTERNAL_CONTENT_URI, values);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * store image file on media store provider
     */
    public static boolean addImageToGallery(File file, Context context) {
        return addImageToGallery(file.getPath(), context);
    }
}
