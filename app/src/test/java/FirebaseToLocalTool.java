

import com.duy.ascii.art.asciiart.model.TextArt;
import com.duy.ascii.art.image.converter.AsciiFileUtil;

import junit.framework.TestCase;

import org.apache.commons.io.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Scanner;
import java.util.regex.Matcher;

/**
 * Created by Duy on 01-May-18.
 */

public class FirebaseToLocalTool extends TestCase {

    public void test() throws IOException, JSONException {
//        File file = new File("./app/src/main/assets/asciiart.json");
//        String content = IOUtils.toString(new FileInputStream(file));
//        JSONObject oldData = new JSONObject(content).getJSONObject("ascii_art");
//        Iterator<String> keys = oldData.keys();
//
//
//        JSONObject newData = new JSONObject();
//        JSONArray array = new JSONArray();
//        newData.put("ascii_art", array);
//        while (keys.hasNext()) {
//            String next = keys.next();
//            JSONObject jsonObject = oldData.getJSONObject(next);
//            array.put(jsonObject);
//        }
//        content = newData.toString(2);
//        FileOutputStream fileOutputStream = new FileOutputStream(new File(file.getParent(), "new_ascii_art.json"));
//        fileOutputStream.write(content.getBytes());
//        System.out.println();
    }

    public void testMerge() throws IOException, JSONException {
        final File file = new File("./app/src/main/assets/asciiart.json");
        final FileInputStream in = new FileInputStream(new File(file.getParent(), "new_ascii_art.json"));
        JSONObject jsonObject = new JSONObject(IOUtils.toString(in));
        JSONArray array = jsonObject.getJSONArray(TextArt.KEY_ROOT);

        Matcher matcher = AsciiFileUtil.PATTERN_SPLIT.matcher(IOUtils.toString(new FileInputStream("./app/src/main/assets/image.txt")));
        while (matcher.find()) {
            String value = matcher.group(2);
            JSONObject item = new JSONObject();
            item.put("category", 0);
            item.put("time", 0L);
            item.put("content", value);
            item.put("name", "");
            item.put("star", 0);
            array.put(item);
        }

        FileOutputStream fileOutputStream = new FileOutputStream(new File(file.getParent(), "new_ascii_art2.json"));
        String content = jsonObject.toString(1);
        fileOutputStream.write(content.getBytes());
    }

    public void testClean() throws IOException, JSONException, InterruptedException {
        final File originalDatabase = new File("./src/test/assets/frist-app-1a7d3-export.json").getCanonicalFile();
        final FileInputStream inputStream = new FileInputStream(originalDatabase);
        JSONObject jsonObject = new JSONObject(IOUtils.toString(inputStream));
        inputStream.close();
        Scanner scanner = new Scanner(System.in);

        JSONObject array = jsonObject.getJSONObject(TextArt.KEY_ROOT);
        HashMap<String, String> contents = new HashMap<String, String>();
        Iterator<String> iter = array.keys();
        while (iter.hasNext()) {
            String key = iter.next();
            JSONObject item = array.getJSONObject(key);
            String content = item.getString("content");
            if (content.matches("[\\w]{1,20}")) {
                iter.remove();
            } else if (content.length() <= 5) {
                iter.remove();
            } else {
                String normalized = content.replaceAll("\\s+", "");
                if (contents.containsKey(normalized)) {
                    iter.remove();
                } else if (content.matches("[A-Za-z \n]{0,20}")) {
                    iter.remove();

                } else if (content.length() > 4000) {

                    //                iter.remove();
                } else {
                    contents.put(normalized, normalized);
                }
            }

            Thread.sleep(100);
            System.out.println(content);
            System.err.println("============================================================== remove?: y/n:");

        }

        File refactoredDatabase = new File(originalDatabase.getParent(), "cleaned_database.json");
        FileOutputStream fos = new FileOutputStream(refactoredDatabase);
        String content = jsonObject.toString(1);
        fos.write(content.getBytes());
        fos.close();
    }
}
